import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import TodoList from './components/TodoList';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
	siteTitle: {
		marginBottom: theme.spacing(2),
	},
}));

const App = () => {

	const classes = useStyles();
	const { siteTitle } = classes;

	return (
		<Container>
			<Typography variant="h1" color="textSecondary" align="center" className={siteTitle}>Todo App</Typography>
			<TodoList />
		</Container>
	)
};

export default App;
