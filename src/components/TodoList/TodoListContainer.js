import { connect } from "react-redux";
import { uiTodoListComponentMounted } from '../../store/actions';
import TodoListComponent from './TodoListComponent';

const mapStateToProps = ({ store: { toDoList } }) => {
	return { toDoList: toDoList }
};

const mapDispatchToProps = dispatch => ({
	dispatchUiTodoListComponentMounted: () => dispatch(uiTodoListComponentMounted()),
});

const TodoListContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(TodoListComponent);

export default TodoListContainer;