import React, { useEffect, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TodoListItemComponent from './TodoListItemComponent';

const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1,
	},
}));

const TodoListComponent = ({ toDoList, dispatchUiTodoListComponentMounted }) => {

	const classes = useStyles();
	const { root } = classes;

	const memoizedComponentMountedCallback = useCallback(() => {
			dispatchUiTodoListComponentMounted();
		},
		[dispatchUiTodoListComponentMounted],
	);

	useEffect(() => {
		memoizedComponentMountedCallback();
	}, [memoizedComponentMountedCallback]);

	return(
		<div className={root}>
			<Grid
				container
				direction="row"
				justify="space-between"
				alignItems="stretch"
				spacing={3}
			>
				{toDoList && toDoList.length ? toDoList.map(todo => {
					const { id, completed, title, userId } = todo;
					return (
						<TodoListItemComponent
							key={id}
							completed={completed}
							id={id}
							title={title}
							userId={userId}
						/>
					)})
				:null}
			</Grid>
		</div>
	)
};

export default TodoListComponent;