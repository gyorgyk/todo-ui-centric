import { call, put } from 'redux-saga/effects';
import { takeLeading } from "../../../store/sagaUtils";
import { UI_TODO_LIST_COMPONENT_MOUNTED } from '../../../store/actionTypes';
import { storeTodoList } from '../../../store/actions';
import { request } from '../../../api/request';
import { GET_TODOS_URL } from '../../../common/constants';

function* getTodoList(){
	try{
		const { data } = yield call(request, GET_TODOS_URL);
		yield put(storeTodoList(data));
	} catch (error) {
		console.warn(error);
	}
}

export function* todoListSagas() {
	yield takeLeading(UI_TODO_LIST_COMPONENT_MOUNTED, getTodoList);
}