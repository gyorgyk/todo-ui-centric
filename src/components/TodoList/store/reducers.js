import { STORE_TODO_LIST } from "../../../store/actionTypes";

export const todoListReducer = (toDoListInState, action) => {
	switch(action.type){
		case STORE_TODO_LIST: {
			const { payload }  = action;
			return [...payload]
		}
		default: return toDoListInState;
	}
};