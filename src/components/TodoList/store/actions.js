import { UI_TODO_LIST_COMPONENT_MOUNTED, STORE_TODO_LIST } from '../../../store/actionTypes';

export const uiTodoListComponentMounted = () => ({ type: UI_TODO_LIST_COMPONENT_MOUNTED });
export const storeTodoList = payload => ({ type: STORE_TODO_LIST, payload });