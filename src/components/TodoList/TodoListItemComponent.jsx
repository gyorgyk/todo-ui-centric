import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
	gridItem: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "space-between",
	},
	paper: {
		padding: theme.spacing(2),
		color: theme.palette.text.secondary,
		height: "100%",
		flexGrow: 1,
	},
	linkStyle: {
		textDecoration: 'none',
		height: "100%",
		flexGrow: 1,
	},
}));

const TodoListItemComponent = ({ completed, id, title, userId}) => {

	const classes = useStyles();
	const { gridItem,  linkStyle } = classes;

	return (
		<Grid item xs={12} sm={6}  md={4} lg={4} xl={4} className={gridItem}>
			<Link to={`todolist-item/${id}`} className={linkStyle}>
				<Paper className={classes.paper}>
					<Typography variant="h6">Task ID: {id}</Typography>
					<Typography variant="h6">Title: {title}</Typography>
					<Typography variant="h6">Status: {completed ? 'Completed' : 'Not Completed'}</Typography>
					<Typography variant="h6">User: {userId}</Typography>
				</Paper>
			</Link>
		</Grid>
	)
};

export default TodoListItemComponent;