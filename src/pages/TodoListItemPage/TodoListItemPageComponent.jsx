import React from 'react';
import { withRouter } from "react-router";
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
	paper: {
		padding: theme.spacing(2),
		minHeight: '100vh',
	},
}));

const TodoListPageComponent = ({ toDoList, match }) => {

	const { params: { id } } = match;
	const classes = useStyles();

	return (toDoList  ? toDoList.filter(todo => parseInt(todo.id) === parseInt(id)).map(todo => {
		const { id, title, completed, userId } = todo;
		return (
			<Container key={todo.id}>
				<Paper className={classes.paper}>
					<Typography variant="h4" align="center" color="textSecondary">Title: {title}</Typography>
					<Typography variant="h6" color="textSecondary">Task ID: {id}</Typography>
					<Typography variant="h6" color="textSecondary">Status: {completed ? 'Completed' : 'Not Completed'}</Typography>
					<Typography variant="h6" color="textSecondary">User: {userId}</Typography>
				</Paper>
			</Container>
		)})
	:null)
};

export default withRouter(TodoListPageComponent);