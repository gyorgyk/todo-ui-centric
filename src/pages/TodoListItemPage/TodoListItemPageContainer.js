import { connect } from "react-redux";
import TodoListItemPageComponent from './TodoListItemPageComponent';

const mapStateToProps = ({ store: { toDoList } }) => {
	return {
		toDoList: toDoList
	}
};

const TodoListContainer = connect(
	mapStateToProps,
	null
)(TodoListItemPageComponent);

export default TodoListContainer;