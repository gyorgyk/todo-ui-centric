import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { rootSaga } from './store/sagas';
import { rootReducer } from './store/reducers';

const reducers = combineReducers({
	store: rootReducer
});

const sagaMiddleware = createSagaMiddleware();

const configureStore = () => {
	const middlewares = [ sagaMiddleware ];
	if (process.env.NODE_ENV !== 'production') {
		middlewares.push(logger);
	}

	const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

	const store = createStore(
		reducers,
		composeEnhancers(applyMiddleware(...middlewares))
	);

	sagaMiddleware.run(rootSaga);

	return store;
};

export default configureStore;