import axios from 'axios';

export const request = url => (
	axios.get(url).then(response => response, error => error
));