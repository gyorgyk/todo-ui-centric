import { todoListReducer } from '../components/TodoList/store/reducers';

const initState = {
	toDoList: []
};

export const rootReducer = (state = initState, action) => {
	return {
		toDoList: todoListReducer(state.toDoList, action)
	};
};