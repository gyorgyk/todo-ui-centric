import { all } from 'redux-saga/effects';
import { todoListSagas } from '../components/TodoList/store/sagas';

export function* rootSaga() {
	yield all([ todoListSagas() ]);
}