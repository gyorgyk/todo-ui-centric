import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#556cd6',
		},
		secondary: {
			main: '#19857b',
		},
		background: {
			default: '#fff',
		},
	},
	text: {
		h1: {
			fontStyle: 'normal',
			fontWeight: 'bold',
			fontSize: '48px',
			lineHeight: '65px'
		},
		h2: {
			fontStyle: 'normal',
			fontWeight: 'bold',
			fontSize: '32px',
			lineHeight: '44px'
		},
		h3: {
			fontStyle: 'normal',
			fontWeight: 'bold',
			fontSize: '22px',
			lineHeight: '30px'
		},
		h4: {
			fontStyle: 'normal',
			fontWeight: 'normal',
			fontSize: '18px',
			lineHeight: '25px'
		},
		h5: {
			fontStyle: 'normal',
			fontWeight: 'normal',
			fontSize: '16px',
			lineHeight: '22px'
		},
		h6: {
			fontStyle: 'normal',
			fontWeight: 'normal',
			fontSize: '14px',
			lineHeight: '19px'
		},
		subtitle: {
			fontStyle: 'normal',
			fontWeight: 'bold',
			fontSize: '32px',
			lineHeight: '44px'
		},
		paragraph: {
			fontStyle: 'normal',
			fontWeight: 'normal',
			fontSize: '20px',
			lineHeight: '27px'
		}
	}
});

export default theme;