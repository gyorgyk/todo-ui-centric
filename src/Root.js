import React from 'react';
import { Provider } from 'react-redux';
import configureStore from './configureStore';
import { ThemeProvider } from "@material-ui/styles";
import theme from "./theme/theme";
import { AppRouter } from './Router/Router'
import CssBaseline from "@material-ui/core/CssBaseline";
import App from './App';

const Store = configureStore();

const Root = () => (
	<Provider store={Store}>
		<ThemeProvider theme={theme}>
			<CssBaseline />
				<AppRouter>
					<App />
				</AppRouter>
		</ThemeProvider>
	</Provider>
);

export default Root;