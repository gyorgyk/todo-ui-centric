import React from "react";
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import App from "../App";
import TodoListItemPage from "../pages/TodoListItemPage";

export const AppRouter = () => {
	return (
		<Router>
			<Switch>
				<Route exact path={`/`} component={ App } />
				<Route path={`/todolist-item/:id`} component={ TodoListItemPage } />
				<Redirect from='*' to='/' />
			</Switch>
		</Router>
	)
};